# Collected Rain Water Calculator
Calculates the rain water collected in the pits of a given landscape.

## Solution Overview
At first the input, from either arguments or from file re-direct,
is read and validated.
Then, if the landscape passed in has more positions than supported an `UnsupportedOperationException` will be thrown.
Similarly, if at a given position an invalid height is given, an `IllegalArgumentException` will be thrown.

Once the given landscape has been validated,
all the water-gathering pit regions are found and add their respective water amounts to get the total.

For finding the pit regions, the landscape is scanned vertically, from top to bottom,
starting from the highest height in the landscape.
For each height level, the landscape is scanned horizontally, from left to right,
if two positions are found, that have ground at that height, and that are not in a previously found region,
they are a new region and are noted down.
This is repeated until all the height-levels have been checked.

The water amount of a pit region is then calculated as the difference between the minimum of the two sides of the region
and the height of each intermediate position.

## How to run
The calculator is processing one landscape at a time. <br/>
It can be executed in various ways from terminal:

### Execute jar file with arguments
```shell
java -jar target/collected-water-calculator-0.0.1-SNAPSHOT.jar 5 2 3 4 5 4 0 3 1
```

### Execute jar file with input file
```shell
java -jar target/collected-water-calculator-0.0.1-SNAPSHOT.jar < input-file
```
where input-file contains the heights either separated by spaces or one on each line

### Execute through Maven
```shell
mvn -q spring-boot:run -Dspring-boot.run.arguments="5 2 3 4 5 4 0 3 1"
```
