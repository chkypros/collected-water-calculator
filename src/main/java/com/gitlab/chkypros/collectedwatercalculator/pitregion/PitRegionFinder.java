package com.gitlab.chkypros.collectedwatercalculator.pitregion;

import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.IntStream;

@Component
public class PitRegionFinder {

    public Set<PitRegion> findPitRegions(final int[] landscape) {
        final var knownPitRegions = new TreeSet<PitRegion>();

        final var highestHeightPositionOptional = getHighestHeightPosition(landscape);
        final var highestHeight = highestHeightPositionOptional
                .map(position -> landscape[position])
                .orElse(0);

        for (int h = highestHeight; h > 0; h--) {
            int position = -1;
            Integer regionStart = null;

            while (position < landscape.length - 1) {
                var previousPosition = position;
                position = getNextCheckPosition(position + 1, knownPitRegions);

                if (position > previousPosition + 1) {
                    regionStart = null;
                }

                if (landscape[position] >= h) {
                    if (null != regionStart && position > regionStart + 1) {
                        knownPitRegions.add(new PitRegion(regionStart, position));
                    }
                    regionStart = position;
                }
            }
        }

        return knownPitRegions;
    }

    private Optional<Integer> getHighestHeightPosition(int[] landscape) {
        return IntStream.range(0, landscape.length).boxed()
                .max(Comparator.comparingInt(k -> landscape[k]));
    }

    private int getNextCheckPosition(final int startPosition, final TreeSet<PitRegion> knownPitRegions) {
        int position = startPosition;
        for (PitRegion pitRegion : knownPitRegions) {
            if (pitRegion.start <= position && position < pitRegion.end) {
                position = pitRegion.end;
                continue;
            }
            break;
        }
        return position;
    }
}
