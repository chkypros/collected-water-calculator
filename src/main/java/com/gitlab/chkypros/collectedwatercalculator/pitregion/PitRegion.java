package com.gitlab.chkypros.collectedwatercalculator.pitregion;

public class PitRegion implements Comparable<PitRegion> {
    public final int start;
    public final int end;

    public PitRegion(int start, int end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public int compareTo(final PitRegion other) {
        return Integer.compare(this.start, other.start);
    }

    @Override
    public String toString() {
        return "(" + start + ", " + end + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PitRegion pitRegion = (PitRegion) o;

        if (start != pitRegion.start) return false;
        return end == pitRegion.end;
    }

    @Override
    public int hashCode() {
        int result = start;
        result = 31 * result + end;
        return result;
    }
}
