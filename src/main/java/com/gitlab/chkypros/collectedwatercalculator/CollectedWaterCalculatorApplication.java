package com.gitlab.chkypros.collectedwatercalculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CollectedWaterCalculatorApplication {
    public static void main(String[] args) {
        SpringApplication.run(CollectedWaterCalculatorApplication.class, args);
    }
}
