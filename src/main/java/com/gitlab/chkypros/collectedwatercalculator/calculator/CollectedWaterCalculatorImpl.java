package com.gitlab.chkypros.collectedwatercalculator.calculator;

import com.gitlab.chkypros.collectedwatercalculator.pitregion.PitRegion;
import com.gitlab.chkypros.collectedwatercalculator.pitregion.PitRegionFinder;
import com.gitlab.chkypros.collectedwatercalculator.validation.LandscapeValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.IntStream;

@Component
public class CollectedWaterCalculatorImpl implements CollectedWaterCalculator {

    private final LandscapeValidator landscapeValidator;
    private final PitRegionFinder pitRegionFinder;

    // Temporary variables
    private int[] landscape;

    @Autowired
    public CollectedWaterCalculatorImpl(
            final LandscapeValidator landscapeValidator,
            final PitRegionFinder pitRegionFinder) {
        this.landscapeValidator = landscapeValidator;
        this.pitRegionFinder = pitRegionFinder;
    }

    @Override
    public long calculateWaterAmount(final int[] landscape) {
        landscapeValidator.validate(landscape);
        this.landscape = landscape;

        final var pitRegions = pitRegionFinder.findPitRegions(landscape);

        return calculateWaterInPitRegions(pitRegions);
    }

    private long calculateWaterInPitRegions(final Set<PitRegion> pitRegions) {
        return pitRegions.stream().mapToLong(this::calculateWaterInPitRegion).sum();
    }

    private long calculateWaterInPitRegion(final PitRegion pitRegion) {
        final var waterLevel = Math.min(landscape[pitRegion.start], landscape[pitRegion.end]);

        return IntStream.range(pitRegion.start + 1, pitRegion.end)
                .map(position -> waterLevel - landscape[position])
                .sum();
    }
}
