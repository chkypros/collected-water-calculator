package com.gitlab.chkypros.collectedwatercalculator.calculator;

public interface CollectedWaterCalculator {
    long calculateWaterAmount(int[] landscape);
}
