package com.gitlab.chkypros.collectedwatercalculator.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class LandscapeValidator {
    private final int maxPositions;
    private final int minHeight;
    private final int maxHeight;

    @Autowired
    public LandscapeValidator(
            @Value("${landscape.positions.max}") int maxPositions,
            @Value("${landscape.height.min}") int minHeight,
            @Value("${landscape.height.max}") int maxHeight) {
        this.maxPositions = maxPositions;
        this.minHeight = minHeight;
        this.maxHeight = maxHeight;
    }

    public void validate(final int[] landscape) {
        if (null == landscape) {
            throw new IllegalArgumentException("Provided landscape argument cannot be null");
        }

        if (landscape.length > maxPositions) {
            throw new UnsupportedOperationException("Cannot process landscapes of more than " + maxPositions + " positions");
        }

        Arrays.stream(landscape)
                .filter(height -> height < minHeight || height > maxHeight)
                .findAny()
                .ifPresent(_invalidHeight -> {
                    String errorMessage = String.format("Each position in the landscape must be between %d and %d",
                            minHeight, maxHeight);
                    throw new IllegalArgumentException(errorMessage);
                });
    }
}
