package com.gitlab.chkypros.collectedwatercalculator;

import com.gitlab.chkypros.collectedwatercalculator.calculator.CollectedWaterCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

@Component
public class CollectedWaterCalculatorCommandLineRunner implements CommandLineRunner {
    private final CollectedWaterCalculator collectedWaterCalculator;

    @Autowired
    public CollectedWaterCalculatorCommandLineRunner(CollectedWaterCalculator collectedWaterCalculator) {
        this.collectedWaterCalculator = collectedWaterCalculator;
    }

    @Override
    public void run(String... args) {
        int[] heights = Arrays.stream(getArguments(args))
                .mapToInt(Integer::valueOf)
                .toArray();

        long waterAmount = collectedWaterCalculator.calculateWaterAmount(heights);

        System.out.println(waterAmount);
    }

    private String[] getArguments(String[] args) {
        if (null != args && args.length > 0) {
            return args;
        }

        try (Scanner scanner = new Scanner(System.in)) {
            if (System.in.available() > 0) {
                return scanner.tokens().toArray(String[]::new);
            }
        } catch (IOException e) {
            // Failed to read from input, do nothing
        }

        return new String[0];
    }
}
