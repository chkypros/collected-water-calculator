package com.gitlab.chkypros.collectedwatercalculator.validation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertThrows;

class LandscapeValidatorTest {
    private static final int MAX_POSITIONS = 20;
    private static final int MIN_HEIGHT = 0;
    private static final int MAX_HEIGHT = 100;

    private LandscapeValidator validator;

    @BeforeEach
    void setUp() {
        validator = new LandscapeValidator(MAX_POSITIONS, MIN_HEIGHT, MAX_HEIGHT);
    }

    @Test
    void calculate_invalid_landscape_size() {
        // Given
        int[] landscape = IntStream.range(0, MAX_POSITIONS + 1).toArray();

        assertThrows(UnsupportedOperationException.class,
                () -> validator.validate(landscape));
    }

    @ParameterizedTest
    @ValueSource(ints = { MIN_HEIGHT - 1, MAX_HEIGHT + 1 })
    void calculate_invalid_landscape_height(int height) {
        // Given
        int[] landscape = { height };

        assertThrows(IllegalArgumentException.class,
                () -> validator.validate(landscape));
    }
}
