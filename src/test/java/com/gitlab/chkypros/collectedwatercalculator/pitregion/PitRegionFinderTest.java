package com.gitlab.chkypros.collectedwatercalculator.pitregion;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class PitRegionFinderTest {

    @InjectMocks
    private PitRegionFinder finder;

    @Test
    void findPitRegions_empty_landscape() {
        // Given
        int[] landscape = {};

        // When
        final var pitRegions = finder.findPitRegions(landscape);

        // Then
        assertEquals(0, pitRegions.size());
    }

    @Test
    void findPitRegions_zero_pits() {
        // Given
        int[] landscape = {1, 2, 2, 3, 1};

        // When
        final var pitRegions = finder.findPitRegions(landscape);

        // Then
        assertEquals(0, pitRegions.size());
    }

    @Test
    void findPitRegions_single_pit() {
        // Given
        int[] landscape = {4, 2, 2, 4, 1};

        // When
        final var pitRegions = finder.findPitRegions(landscape);

        // Then
        assertEquals(1, pitRegions.size());
        assertTrue(pitRegions.contains(new PitRegion(0, 3)));
    }

    @Test
    void findPitRegions_multiple_pits() {
        // Given
        int[] landscape = {4, 2, 2, 4, 1, 3, 0, 2};

        // When
        final var pitRegions = finder.findPitRegions(landscape);

        // Then
        assertEquals(3, pitRegions.size());
        assertTrue(pitRegions.containsAll(List.of(new PitRegion(0, 3), new PitRegion(3, 5))));
    }
}
