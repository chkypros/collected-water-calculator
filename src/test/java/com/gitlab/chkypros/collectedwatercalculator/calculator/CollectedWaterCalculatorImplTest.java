package com.gitlab.chkypros.collectedwatercalculator.calculator;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(properties = {
        "landscape.positions.max=32000",
        "landscape.height.min=0",
        "landscape.height.max=32000"
})
class CollectedWaterCalculatorImplTest {

    @Autowired
    private CollectedWaterCalculatorImpl calculator;

    @Test
    void calculate_empty_landscape() {
        // When
        long waterAmount = calculator.calculateWaterAmount(new int[0]);

        // Then
        assertEquals(0, waterAmount);
    }

    @Test
    void calculate_single_hill() {
        // Given
        int[] landscape = {0, 1, 2, 3, 0};

        // When
        long waterAmount = calculator.calculateWaterAmount(landscape);

        // Then
        assertEquals(0, waterAmount);
    }

    @Test
    void calculate_single_pit() {
        // Given
        int[] landscape = {4, 3, 2, 1, 4};

        // When
        long waterAmount = calculator.calculateWaterAmount(landscape);

        // Then
        assertEquals(6, waterAmount);
    }

    @Test
    void calculate_double_pit() {
        // Given
        int[] landscape = {5, 2, 3, 4, 5, 4, 0, 3, 1};

        // When
        long waterAmount = calculator.calculateWaterAmount(landscape);

        // Then
        assertEquals(9, waterAmount);
    }
}
